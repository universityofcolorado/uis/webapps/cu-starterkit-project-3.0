# CU StarterKit 3.0 Pantheon Upstream

This is Pantheon's recommended starting point for forking new [Drupal](https://www.drupal.org/) upstreams
that work with the Platform's Integrated Composer build process.

For more information and detailed installation guides, please visit the
Integrated Composer Pantheon documentation: https://pantheon.io/docs/integrated-composer

## Features

- [CU StarterKit Profile](web/profiles/custom/cu_starterkit_profile/README.md)
- [CU StarterKit Theme](web/themes/custom/cu_starterkit_theme/README.md)

